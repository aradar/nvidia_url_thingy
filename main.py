#!/usr/bin/env python3

import copy
from datetime import datetime
import json
import random
import time
from pathlib import Path
from typing import Dict, List, Tuple

import requests as requests


def get_nvidia_search_dict() -> Dict:
    url = "https://api.nvidia.partners/edge/product/search?page=1&limit=9&locale=de-de&category=GPU&manufacturer=NVIDIA"
    session = requests.Session()
    req = session.get(
        url,
        # a browser user agent needs to be set as nvidia is a bunch of dickheads
        headers={
            "user-agent": "Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36"
        })
    return req.json()


def extract_prod_to_store_id_map(search_result_dict: Dict) -> Dict[str, str]:
    prod_to_store_id = dict()
    details = list()
    details.append(search_result_dict["searchedProducts"]["featuredProduct"])
    details.extend(search_result_dict["searchedProducts"]["productDetails"])
    for det in details:
        retailers = det["retailers"]
        for ret in retailers:
            purchase_link = ret["purchaseLink"]
            if not purchase_link.startswith("https://www.notebooksbilliger.de"):
                continue
            prod_to_store_id[det["displayName"]] = "+".join(purchase_link.rsplit("+")[-2:])

    return prod_to_store_id


def load_prod_map_history(history_file: Path) -> Dict[str, List[str]]:
    if history_file.exists():
        with history_file.open("r") as f:
            return json.load(f)
    return dict()


def find_changed_prods(prod_map: Dict, prod_map_history: Dict) -> List:
    changed_prods = list()
    for p_name, p_id in prod_map.items():
        p_history = prod_map_history.get(p_name, dict())

        latest_id = p_history.get("latest_id")
        if p_id == latest_id:
            continue

        changed_prods.append((p_name, p_id))

    return changed_prods


def update_prod_map_history(
        history_file: Path,
        history: Dict,
        changed_prods: List[Tuple[str, int]]) \
        -> Dict[str, List[str]]:

    new_hist = copy.deepcopy(history)
    for p_name, p_id in changed_prods:

        if not p_name in new_hist:
            new_hist[p_name] = dict()
            new_hist[p_name]["old_ids"] = list()

        new_hist[p_name]["latest_id"] = p_id
        new_hist[p_name]["old_ids"].append(p_id)

    with history_file.open("w") as f:
        json.dump(new_hist, f, indent=4)

    return new_hist


def print_changed_prods(changed_prods: List[Tuple[str, int]]):
    print(f"The following products have changed ({datetime.now()}):")
    for p_name, p_id in changed_prods:
        print(" -", p_name, p_id)


if __name__ == "__main__":
    prod_map_history_file = Path(".prod_map_history.json")
    prod_map_history = load_prod_map_history(prod_map_history_file)

    if prod_map_history:
        print("Last used and recorded product ids are:")
        for p_name, p_hist_dict in prod_map_history.items():
            print(" -", p_name, p_hist_dict["latest_id"])

    while True:
        prod_map = extract_prod_to_store_id_map(get_nvidia_search_dict())
        changed_prods = find_changed_prods(prod_map, prod_map_history)

        if changed_prods:
            prod_map_history = update_prod_map_history(
                prod_map_history_file,
                prod_map_history,
                changed_prods)

            print_changed_prods(changed_prods)

        time.sleep(random.uniform(45, 75))
